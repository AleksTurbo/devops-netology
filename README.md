# devops-netology
devops-netology
aleksturbo
aleksturbo@yandex.ru
TeSt 05.05.2022
12:58


#Terraform
**/.terraform/* - игнорируется папка .terraform в любом месте репозитори
*.tfstate
*.tfstate.*любые фаилы с часть имени .tfstate в текущем каталоге
crash.log
crash.*.log - игнорируются любые фаилы с часть имени crash.log в текущем каталоге

*.tfvars
*.tfvars.json - игнорируются любые фаилы с частю имени .tfvars и их разновидности в текущем каталоге

override.tf
override.tf.json
*_override.tf
*_override.tf.json - игнорируются любые фаилы с частю имени override.tf и их разновидности в текущем каталоге

.terraformrc
terraform.rc - игнорируются любые фаилы с частю имени .terraformrc в текущем каталоге
